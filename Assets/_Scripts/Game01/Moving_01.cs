﻿using UnityEngine;
using System.Collections;

public class Moving_01 : MonoBehaviour
{
    public float speed = 5;

    void Start()
    {

    }

    void Update()
    {
        transform.Translate(Vector3.left * speed * Time.deltaTime);
    }


    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.name == "DestroyPoint")
        {
            Vector3 newPos = this.gameObject.transform.position;
            newPos.x = Controller_01.restorePosition.x;
            transform.position = newPos;
        }
    }
}
