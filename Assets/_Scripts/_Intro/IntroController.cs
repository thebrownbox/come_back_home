﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

/// <summary>
/// Quản lý scene Intro
/// </summary>
public class IntroController : MonoBehaviour
{
    public DongHo dongHo;

    public GameObject MenuScene;
    public Transform MenuScene_Pos;
    public GameObject MenuPanel;
    public Transform MenuPanel_Pos;

    private string xx = "Thay đổi một đôgns thứ đây có làm sao đâu! : | Bó tay";

    // Use this for initialization
    void Start()
    {
        dongHo.onComplete = StartMenuScene;
    }

    /// <summary>
    /// Hiển thị cảnh intro
    /// </summary>
    void StartIntroScene()
    {
        int hoang = 0;
    }

    /// <summary>
    /// Hiển thị giao diện start
    /// </summary>
    void StartMenuScene()
    {
        MenuScene.transform.DOMove(MenuScene_Pos.position, 3);
        MenuPanel.transform.DOMove(MenuPanel_Pos.position, 3);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Click_Pause()
    {
        Application.LoadLevel(Application.loadedLevel);
    }
}
