﻿using UnityEngine;
using System.Collections;

/**
 * Các hàm trợ giúp hay được sử dụng trong game
 * */
public class Helper
{
    #region Các hàm tính toán liên quan đến tranform

    /// <summary>
    /// Chuyển góc bình thường về goc Euler
    /// </summary>
    public static Vector3 ToEuler(Vector3 goc)
    {
        Vector3 e = new Vector3();
        Quaternion q = Quaternion.Euler(goc);
        e.x = q.x;
        e.y = q.y;
        e.z = q.z;
        return e;
    }

    #endregion




}
